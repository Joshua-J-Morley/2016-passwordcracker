﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace PasswordRecoveryBizService
{
	///*********************************************************///
	///	<summary>  Interface for worker to implement </summary> ///
	///*********************************************************///
	[ServiceContract]
	public interface IPRBizServiceController
	{
		///***********************************************************************************///
		/// <summary> called asynchronously used to check combination for password </summary> ///
		///   <param name="passwords">list of permutations of possible passwords </param>     ///
		///    <param name="encrypted">encrypted password to compare and recover</param>      ///
		///         <returns> the plaintext of the recovered password   </returns>            ///
		///***********************************************************************************///
		[OperationContract]
		string FindPassword(List<string> passwords, string encrypted);

		///************************************************************************************///
		/// <summary> callback method for when given permutations have been checked </summary> ///
		/// <param name="iResult"> object that stores AsyncResult object and delegate</param>  ///
		///   <remarks> callback will also be called when passowrd has been found </remarks>   ///
		///************************************************************************************///
		[OperationContract]
		void PasswordRecoveryComplete(IAsyncResult iResult);
	}
}
