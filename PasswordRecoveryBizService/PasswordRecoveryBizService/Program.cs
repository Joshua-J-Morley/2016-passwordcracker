﻿using PasswordRecoveryBizDLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace PasswordRecoveryBizService
{
	///**************************************************************///
	/// <summary> class containing main for the workers </summary>   ///
	///**************************************************************///
	class Program
	{
		///*****************************************************************///
		/// <summary> set up connection to manager via tcp url </summary>   ///
		///*****************************************************************///
		static void Main(string[] args)
		{
			IWorker mPassManager;
			DuplexChannelFactory<IWorker> managerFactory;
			NetTcpBinding tcpBinding = new NetTcpBinding();
			PRBizServiceControllerImpl callback;

			//create reference to an instance of this class to be used as callback
			callback = new PRBizServiceControllerImpl();

			///.net has restrictions on max size of message and num elements in array
			///increase max size of messages
			tcpBinding.MaxReceivedMessageSize = System.Int32.MaxValue;
			///increase max number of elements in array
			tcpBinding.ReaderQuotas.MaxArrayLength = System.Int32.MaxValue;

			///connect to manager server
			string sURL = "net.tcp://localhost:5002/RecoveryManager2"; 

			///try create controller factory
			try
			{
				///duplex channel factory passing instance of this class as callback handler
				managerFactory = new DuplexChannelFactory<IWorker>(callback, tcpBinding, sURL);

				///ensure dataControllerFactory sucessfully initialized
				if (managerFactory != null)
				{
					///create channel to initialize TMDataController
					mPassManager = managerFactory.CreateChannel();

					///add worker to managers list
					mPassManager.AddWorker();

					//IClientChannel contextChannel = mPassManager as IClientChannel;
					//contextChannel.OperationTimeout = TimeSpan.FromDays(10);


					///block main finishing until user enters
					Console.WriteLine("Press Enter to Exit"); 
					Console.ReadLine();

					try///temporary fix until i figure out why workers time out, see report
					{
						mPassManager.RemoveWorker();
					}
					catch (CommunicationObjectFaultedException communicationError)
					{
						Console.Write("Manager Object connection lost");
						Console.WriteLine(communicationError.Message);
					}
				}
			}
			///catch argument null exception
			catch (ArgumentNullException addressIsNull)
			{
				Console.WriteLine("\n\n address for channel factory is null:\n" + addressIsNull);
			}

			
		}
	}
}
