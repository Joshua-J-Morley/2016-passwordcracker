﻿using PasswordRecoveryBizDLL;
using PwdRecover;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Remoting.Messaging;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace PasswordRecoveryBizService
{
	///delegate to be used to async call find password method
	public delegate string findPasswordDelegate(List<string> passwords, string encrypted);

	///***********************************************************************************///
	///    <summary> Password recovery worker, connects to password manager </summary>    ///
	/// <remarks> implements callback interface so manager can communicate back</remarks> ///
	///***********************************************************************************///
	[ServiceBehavior(ConcurrencyMode=ConcurrencyMode.Multiple, UseSynchronizationContext=false)]
	internal class PRBizServiceControllerImpl : IPRBizServiceController, IWorkerCallback
	{
		///************************************************///
		///************************************************///
		///	      <summary> Member Fields </summary>       ///
		///************************************************///
		///************************************************///
		///used to access password services dll contents
		private PasswordServices mPasswordServices;


		///*******************************************************///
		///*******************************************************///
		///	      <summary> standard class methods </summary>     ///
		///*******************************************************///
		///*******************************************************///

		///**********************************************************///
		///   <summary> Default constructor for worker </summary>    ///
		///**********************************************************///
		public PRBizServiceControllerImpl()
		{
			mPasswordServices = new PasswordServices();
		}


		///************************************************///
		///************************************************///
		///	<summary> Password Recovery Methods </summary> ///
		///************************************************///
		///************************************************///

		///******************************************************************************///
		/// <summary> called by manager used to begin recovery asynchronously </summary> ///
		/// <param name="passwords">list of permutations of possible passwords </param>  ///
		///  <param name="encrypted">encrypted password to compare and recover</param>   ///
		///  <remarks> Method is called by manager and will asynchronously locally call 
		///  FindPassword so that managers network call can return and manager
		///  can continue execution without any asynchronous calls over network</remarks>///
		///******************************************************************************///
		public void FindPasswordAsync(List<string> passwords, string encrypted)
		{
			//Console.WriteLine("thread open: " + workerNum);
			findPasswordDelegate findDel;
			AsyncCallback callback;
			IWorker state = OperationContext.Current.GetCallbackChannel<IWorker>();

			findDel = this.FindPassword;
			callback = this.PasswordRecoveryComplete;

			findDel.BeginInvoke(passwords, encrypted, callback, state);
		}

		///***********************************************************************************///
		/// <summary> called asynchronously used to check combination for password </summary> ///
		///   <param name="passwords">list of permutations of possible passwords </param>     ///
		///    <param name="encrypted">encrypted password to compare and recover</param>      ///
		///         <returns> the plaintext of the recovered password   </returns>            ///
		///***********************************************************************************///
		public string FindPassword(List<string> passwords, string encrypted)
		{
			string retString = null;
			
			///check each permutation in the given list
			foreach(string password in passwords)
			{
				//Console.WriteLine(password);

				///encrypt permutation
				string encPassword = mPasswordServices.EncryptString(password);
				
				///check if this permutation equals password to recover
				if (encPassword.Equals(encrypted))
				{
					///password has been found, break iterator
					Console.WriteLine("Comparing: \n" + encPassword + "\n" + encrypted);
					Console.WriteLine("success");
					retString = password;
					break;
				}
			}

			///return plaintext password
			return retString;
		}

		///************************************************************************************///
		/// <summary> callback method for when given permutations have been checked </summary> ///
		/// <param name="iResult"> object that stores AsyncResult object and delegate</param>  ///
		///   <remarks> callback will also be called when passowrd has been found </remarks>   ///
		///************************************************************************************///
		public void PasswordRecoveryComplete(IAsyncResult iResult)
		{
			string resString;

			///get async result object and callback reference from imported interface
			findPasswordDelegate recover;
			IWorker callback;
			AsyncResult asyncObj = (AsyncResult)(iResult);
			callback = (IWorker)(asyncObj.AsyncState);

			///ensure endinvoke has not been already called
			if (asyncObj.EndInvokeCalled == false)
			{
				///get delegate out of asyncResult object and end the invokation
				recover = (findPasswordDelegate)asyncObj.AsyncDelegate;

				try///temporary fix until i figure out why workers time out, see report
				{
					resString = recover.EndInvoke(asyncObj);

					///callback to manager with recovered plaintext or null
					callback.OnRecoveryComplete(resString);
				}
				catch (ObjectDisposedException communicationError)
				{
					Console.Write("Manager Object connection lost");
					Console.WriteLine(communicationError.Message);
				}
			}

			///close async wait handle
			asyncObj.AsyncWaitHandle.Close();

		}
	}
}
