﻿using PasswordRecoveryBizDLL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PasswordRecoveryGui
{
	///delegate to be used to async call password recovery method
	public delegate string RecoverPasswordDelegate();
	///delegate to be used to async call progress update methods
	public delegate void UpdateProgressBarDelegate();

	///***********************************************************************************///
	/// <summary> Password recovery windows GUI, connects to password manager </summary>  ///
	/// <remarks> implements callback interface so manager can communicate back</remarks> ///
	///***********************************************************************************///
	[ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple,UseSynchronizationContext = false)]
	public partial class MainWindow : Window, IManagerGuiCallback
	{
		///delegates to be used by dispatcher to update gui
		protected delegate void DisplayDecryptedPasswordDel();
		protected delegate void DisplayPercentageDel();

		///************************************************///
		///************************************************///
		///	      <summary> Member Fields </summary>       ///
		///************************************************///
		///************************************************///
		///used to communicate with password manager 
		private IManagerGui mPassManager;
		///length in characters of password to recover
		private int mPasswordLength;
		///string storing encrypted password to crack
		private string mEncryptedPassword;
		///string storing recovered password in plaintext
		private string mDecryptedPassword;
		///number between 1 - 100 indicating number of segments procssed
		private int mPercentageComplete;
		///actual number of segments or combinations processed
		private int mProcessedSegments;
		///total number of segments
		private int mTotalSegments;


		///*******************************************************///
		///*******************************************************///
		///	      <summary> standard class methods </summary>     ///
		///*******************************************************///
		///*******************************************************///

		///***************************************************************///
		///   <summary> Default constructor for main window </summary>    ///
		///***************************************************************///
		public MainWindow()
		{
			///generated method, initialise gui components
			InitializeComponent();

			///initialise member fields
			mPassManager = null;
			mPasswordLength = 0;
			mEncryptedPassword = "";
			mDecryptedPassword = "";
			mPercentageComplete = 0;
			mProcessedSegments = 0;
			mTotalSegments = 0;
		}

		/* *********************************** *
		 * *********************************** *
		 * ******** Components Loaded ******** *
		 * *********************************** *
		 * *********************************** */

		///****************************************************************************///
		/// <summary> called when main window is loaded, connect to manager </summary> ///
		///****************************************************************************///
		private void Window_Loaded(object sender, RoutedEventArgs e)
		{
			InitialiseManagerConnection();
		}


		///************************************************///
		///************************************************///
		///	    <summary> GUI event handlers </summary>    ///
		///************************************************///
		///************************************************///

		///******************************************************///
		///	  <summary> length button event handler </summary>   ///
		///******************************************************///
		private void btn_Length_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				///set to member field
				mPasswordLength = Int32.Parse(txtBox_length.Text);

				///validate password length
				if (mPasswordLength < 1)
				{
					System.Windows.Forms.MessageBox.Show("Please Enter a number > 0");

					///set to member field to 0 to not bypass next input validation
					mPasswordLength = 0;
				}
				///if length is valid
				else
				{
					///ensure manager connection was successful
					if (mPassManager != null)
					{
						//try set the password length
						try
						{
							mPassManager.SetPasswordSize(mPasswordLength);
						}
						//catch fault exception from server if methods fail
						catch (FaultException setLengthError)
						{
							Console.WriteLine("\n set length fault:" + setLengthError);
						}
						///Issue Communicating with manager
						catch (CommunicationObjectFaultedException setLengthError)
						{
							Console.WriteLine("\nset length communication fault:" + setLengthError);
						}
					}
					///if connection was not established successfully
					else
					{
						//attempt to re connect
						InitialiseManagerConnection();
					}
				}
			}
			catch (FormatException wrongFormatException)
			{
				System.Windows.Forms.MessageBox.Show("Please Enter a number for password length");
			}
			catch (ArgumentNullException argNullException)
			{
				System.Windows.Forms.MessageBox.Show("Please enter a number for password length");
			}
			
		}

		///************************************************************************///
		///	  <summary> next password to recover button event handler </summary>   ///
		///************************************************************************///
		private void btn_GeneratePass_Click(object sender, RoutedEventArgs e)
		{
			///ensure manager connection was successful
			if (mPassManager != null)
			{
				//try get next encrytped password to recover
				try
				{
					mEncryptedPassword = mPassManager.NextEncryptedPasswordToRecover();
					txtBox_generatePass.Text = mEncryptedPassword;
				}
				//catch fault exception from server if methods fail
				catch (FaultException nextEncryptedPass)
				{
					Console.Write("\n\n next encrypted password to recover fault:");
					Console.WriteLine(nextEncryptedPass);
				}
				///Issue Communicating with manager
				catch (CommunicationObjectFaultedException nextEncryptedPass)
				{
					Console.WriteLine("\nnext password communication fault:" + nextEncryptedPass);
				}
			}
			///if connection was not established successfully
			else
			{
				//attempt to re connect
				InitialiseManagerConnection();
			}
			
		}

		///*********************************************************///
		///	  <summary> recover password event handler </summary>   ///
		///*********************************************************///
		private void btn_RunRecovery_Click(object sender, RoutedEventArgs e)
		{
			RecoverPasswordDelegate recoverDel;
			AsyncCallback callback;

			///reset percentage complete and progressbar
			mPercentageComplete = 0;
			progressBar.Value = 0;

			///asynchronously call FindPassword
			recoverDel = this.FindPassword;
			callback = this.PasswordRecoveredOnComplete;
			recoverDel.BeginInvoke(callback, null);
		}

		///***************************************************************///
		///	  <summary> cancel recovery button event handler </summary>   ///
		///***************************************************************///
		private void btn_CancelRecovery_Click(object sender, RoutedEventArgs e)
		{
			///ensure manager connection was successful
			if (mPassManager != null)
			{
				//try to cancel recovery
				try
				{
					mPassManager.CancelOperation();
					System.Windows.Forms.MessageBox.Show("Operation Cancelled");
					progressBar.Value = 0;
				}
				//catch fault exception from server if methods fail
				catch (FaultException cancelFault)
				{
					Console.Write("\n\n cancel recovery fault:\n");
					Console.WriteLine(cancelFault);
				}
				///Issue Communicating with manager
				catch (CommunicationObjectFaultedException cancelFault)
				{
					Console.WriteLine("\n cancel recovery communication fault:" + cancelFault);
				}
			}
			///if connection was not established successfully
			else
			{
				//attempt to re connect
				InitialiseManagerConnection();
			}
		}


		///************************************************///
		///************************************************///
		///	<summary> Recover Password methods </summary> ///
		///************************************************///
		///************************************************///


		///****************************************************************************///
		///	  <summary> used to call begin password recovery in manager </summary>     ///
		///            <returns> plaintext password recovered </returns>               ///
		///****************************************************************************///
		public string FindPassword()
		{
			///default the returned string to empty
			string decryptedPass = "";

			///call method which will begin progress updates
			UpdateProgressBarAsync();

			///ensure manager connection was successful
			if (mPassManager != null)
			{
				//try get decrytped password
				try
				{
					decryptedPass = mPassManager.BeginRecovery();
				}
				//catch fault exception from server if methods fail
				catch (FaultException beginRecoveryFault)
				{
					Console.Write("\n\n begin recovery fault:");
					Console.WriteLine(beginRecoveryFault);
				}
				///Issue Communicating with manager
				catch (CommunicationObjectFaultedException beginRecoveryFault)
				{
					Console.WriteLine("\n recover communication fault:" + beginRecoveryFault);
				}
			}
			///if connection was not established successfully
			else
			{
				//attempt to re connect
				InitialiseManagerConnection();
			}
			
			return decryptedPass;
		}

		//callback method used by delegate once password has been recovered
		///************************************************************************************///
		///      <summary> callback method for when password has been recovered </summary>     ///
		/// <param name="iResult"> object that stores AsyncResult object and delegate</param>  ///
		///************************************************************************************///
		public void PasswordRecoveredOnComplete(IAsyncResult iResult)
		{
			string resString;

			///get async result object from imported interface
			RecoverPasswordDelegate findDel;
			AsyncResult asyncObj = (AsyncResult)(iResult);

			///ensure endinvoke has not been already called
			if (asyncObj.EndInvokeCalled == false)
			{
				///get delegate out of asyncResult object and end the invokation
				findDel = (RecoverPasswordDelegate)asyncObj.AsyncDelegate;
				resString = findDel.EndInvoke(asyncObj);

				///update member field and call method to update gui
				mDecryptedPassword = resString;
				PasswordRecoveredDispatcher(resString);
			}

			///close async wait handle
			asyncObj.AsyncWaitHandle.Close();
		}

		///******************************************************************************///
		///	   <summary> used to update the gui with the recovered password </summary>   ///
		///   <param name="inDecryptedPassword"> plaintext password recovered</param>    ///
		///******************************************************************************///
		public void PasswordRecoveredDispatcher(string inDecryptedPassword)
		{
			///assign method to delegate and use dispatcher to invoke
			DisplayDecryptedPasswordDel decryptedPassDel;
			decryptedPassDel = DisplayDecryptedPassword;
			Application.Current.Dispatcher.Invoke(decryptedPassDel);
		}

		///********************************************************************************///
		///	 <summary> used by dispatcher to update gui with recovered password </summary> ///
		///********************************************************************************///
		public void DisplayDecryptedPassword()
		{
			///update member field and txtbox
			mPercentageComplete = 100;
			txtBox_recoveredPass.Text = mDecryptedPassword;
		}


		///************************************************///
		///************************************************///
		///	    <summary> Progress handlers </summary>     ///
		///************************************************///
		///************************************************///

		///****************************************************************///
		///	  <summary> used to asynchronously update progress </summary>  ///
		///****************************************************************///
		public void UpdateProgressBarAsync()
		{
			UpdateProgressBarDelegate updateDel;
			AsyncCallback callback;

			///set delegate to address of update method and invoke async
			updateDel = UpdateProgressBar;
			callback = UpdateProgressBarOnComplete;
			updateDel.BeginInvoke(callback, null);
		}

		///*********************************************************///
		///	  <summary> used to update progress to user </summary>  ///
		///*********************************************************///
		public void UpdateProgressBar()
		{
			///ensure manager connection was successful
			if(mPassManager != null)
			{
				//try to subscribe for notifications
				try
				{
					mPassManager.SubscribeNotifications();
				}
				//catch fault exception from server if methods fail
				catch (FaultException subscribeFault)
				{
					Console.Write("\n\n subscribe for notifications fault:\n");
					Console.WriteLine(subscribeFault);
				}
				///Issue Communicating with manager
				catch (CommunicationObjectFaultedException subscribeFault)
				{
					Console.WriteLine("\n subscribe communication fault:\n" + subscribeFault);
				}
			}
			///if connection was not established successfully
			else
			{
				//attempt to re connect
				InitialiseManagerConnection();
			}
		}

		///*****************************************************************************///
		///	       <summary> used to notify gui of recovery progress </summary>         ///
		/// <param name="inProcessed"> number of segments/combinations checked </param> ///
		///   <param name="inTotal"> total number of segments or combinations </param>  ///
		///   <remarks> Segments is used as unit of measurement unless password length
		///   is 1 or 2 characters for segment size of 10 000 </remarks>                ///
		///*****************************************************************************///
		public void NotifyGui(double inProcessed, double inTotal)
		{
			///update member fields
			mProcessedSegments = (int)inProcessed;
			mTotalSegments = (int)inTotal;

			///initialise delegate for dispatcher to update percent complete
			DisplayPercentageDel percentageCompleteDel;
			percentageCompleteDel = DisplayPercentageComplete;

			///calcualte percent complete and call dispatcher
			mPercentageComplete = (int)((inProcessed / inTotal) * 100);
			Application.Current.Dispatcher.Invoke(percentageCompleteDel);
		}

		///*******************************************************************///
		///	  <summary> used to update gui indicating progress </summary>     ///
		///*******************************************************************///
		public void DisplayPercentageComplete()
		{
			progressBar.Minimum = 0;
			progressBar.Maximum = 100;
			
			///update gui values to show progress
			progressBar.Value = mPercentageComplete;
			lbl_Total.Content = mTotalSegments;

			///this string is reversed (completed/processed) as string is from left on GUI
			lbl_completed.Content = " / " + mProcessedSegments;

			//Console.WriteLine("So many Percentage: " + mPercentageComplete);
		}

		///*************************************************************************************///
		/// <summary> callback method for updateprogress when password has been found </summary>///
		/// <param name="iResult"> object that stores AsyncResult object and delegate</param>   ///
		///*************************************************************************************///
		public void UpdateProgressBarOnComplete(IAsyncResult iResult)
		{
			///get async result object from imported interface
			UpdateProgressBarDelegate progressBarDel;
			AsyncResult asyncObj = (AsyncResult)(iResult);

			///ensure endinvoke has not been already called
			if (asyncObj.EndInvokeCalled == false)
			{
				///get delegate out of asyncResult object and end the invokation
				progressBarDel = (UpdateProgressBarDelegate)asyncObj.AsyncDelegate;
				progressBarDel.EndInvoke(asyncObj);
			}
			///close async wait handle
			asyncObj.AsyncWaitHandle.Close();
		}


		///************************************************************///
		///************************************************************///
		///	<summary> manager-gui Connection initialisation </summary> ///
		///************************************************************///
		///************************************************************///

		///**************************************************************************************///
		///<summary> initialise connection to manager via duplex channel/ nettcpbinding</summary>///
		///**************************************************************************************///
		public void InitialiseManagerConnection()
		{
			DuplexChannelFactory<IManagerGui> managerFact;
			NetTcpBinding tcpBinding = new NetTcpBinding();
			IManagerGuiCallback cbHandler;

			//create reference to an instance of this class to be used as callback
			cbHandler = this;

			//increase max size of messages and number of elements in array
			tcpBinding.MaxReceivedMessageSize = System.Int32.MaxValue;
			tcpBinding.ReaderQuotas.MaxArrayLength = System.Int32.MaxValue;

			string sURL = "net.tcp://localhost:5002/RecoveryManager1";

			//try create controller factory
			try
			{
				//duplex channel factory passing instance of this class as callback handler
				managerFact = new DuplexChannelFactory<IManagerGui>(cbHandler, tcpBinding, sURL);

				//ensure passControllerFactory sucessfully initialized
				if (managerFact != null)
				{
					//create channel to initialize passControllerFactory
					mPassManager = managerFact.CreateChannel();

					//change timeout time to account for larger password recoveries
					IClientChannel contextChannel = mPassManager as IClientChannel;
					contextChannel.OperationTimeout = TimeSpan.FromDays(10);
				}
			}
			//catch argument null exception
			catch (ArgumentNullException addressIsNull)
			{
				Console.WriteLine("\n\n address for channel factory is null:\n" + addressIsNull);

				///set manager reference to null to prevent further errors
				mPassManager = null;
			}
			//catch argument null exception
			catch (Exception otherExceptions)
			{
				Console.WriteLine("\n\nchannel factory returns null:\n" + otherExceptions.Message);

				///set manager reference to null to prevent further errors
				mPassManager = null;
			}
		}
	}
}
