﻿<%@ Page Title="Home Page" Language="C#" AutoEventWireup="true" CodeFile="PasswordRecoveryWeb.aspx.cs" Inherits="_Default" %>


<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title> Password Cracking Page</title>

		<%-- Styles --%>
		<style type="text/css">
			/*locations and sizes of gui elements*/
			#TextArea1 
			{
				height: 67px;
			}
			
			/*location and size of next password button*/
			#btn_NextPasswordToRecover 
			{
				position: absolute;
				float: left;
				top: 74px;
				left: 150px;
				height: 26px;
				width: 169px;
				margin-top: 0px;
			}
			#form1 
			{
				height: 332px;
			}
			/*location and size of password length text*/
			#Text1 
			{
				top: 28px;
				left: 320px;
				position: absolute;
				height: 22px;
				width: 104px;
			}

			/*location and size of password length text box*/
			#txtBox_PasswordLength 
			{
				top: 29px;
				left: 325px;
				position: absolute;
				height: 22px;
				width: 128px;
			}
			
			/*location and size of password length button*/
			#btn_PasswordLengthUpdate 
			{
				top: 29px;
				left: 467px;
				position: absolute;
				height: 26px;
				width: 61px;
			}

			/*location and size of next password to recover text box*/
			#txtArea_NextPasswordToRecover 
			{
				top: 75px;
				left: 341px;
				position: absolute;
				height: 78px;
				width: 235px;
			}

			/*location and size of recover button*/
			#btn_Recover 
			{
				top: 180px;
				left: 385px;
				position: absolute;
				height: 26px;
				width: 54px;
			}

			/*location and size of recovered password text box*/
			#txtArea_RecoveredPassword 
			{
				top: 226px;
				left: 360px;
				position: absolute;
				height: 30px;
				width: 175px;
			}

			/*location and size of next password to recover text box*/
			#txt_NextPasswordToRecover 
			{
				top: 75px;
				left: 340px;
				position: absolute;
				height: 58px;
				width: 190px;
			}

			/*location and size of recovered password text box*/
			#txt_RecoveredPassword 
			{
				top: 229px;
				left: 367px;
				position: absolute;
				height: 22px;
				width: 128px;
			}
		</style>

		<script type="text/javascript">
			//function called to asyncronously find the password
			function FindPasswordAsync(fnOnCompletion)
			{
				req = null; //var emited so variable is global

				try//try to parse XML
				{
					if (window.XMLHttpRequest != undefined)  // check XMLHttpRequest compatability with browser
					{
						req = new XMLHttpRequest(); //if compatable create new request object
					}
					else
					{
						req = new ActiveXObject("Microsoft.XMLHTTP"); //otherwise (IE) create XMLHTTP request for IE
					}
				}
				catch (e)//parse xml failed
				{
					alert("Failed to create XML parser. Error is: \n" + e.message)
				}

				try//try to open connection
				{
					req.onreadystatechange = fnOnCompletion; //initialise pointer to completion callback function
					req.open("POST", "PRWebService.svc", true); //set URL for find password request soap action to go to, true indicates async
				}
				catch (e)//connection opening failed
				{
					alert("Failed to open connection. Error is: \n" + e.message)
				}

				try//try to add request header details
				{
					req.setRequestHeader("Content-Type", "text/xml");//set header coontent type as text/xml
					req.setRequestHeader("SOAPAction", "http://tempuri.org/IPRWebService/RunRecovery"); //soap URL for method to call through interface
				}
				catch (e)//add request header details failed
				{
					alert("Failed to set request header. Error is: \n" + e.message)
				}

				try
				{
					var sMsg = '<?xml version="1.0" encoding="utf-8"?> \
						<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"> \
							<soap:Body> \
								<RunRecovery xmlns="http://tempuri.org/"/> \
							</soap:Body> \
						</soap:Envelope>';
				}
				catch (e)//creating sMsg failed
				{
					alert("Failed to create sMsg. Error is: \n" + e.message)
				}
				try
				{
					req.send(sMsg);
					//alert("sent");
				}
				catch (e)//send message failed
				{
					alert("Failed to send message. Error is: \n" + e.message)
				}
			}

			//function for handling completion callback 
			function FindPasswordRPC_OnCompoletion()
			{
				if (req.readyState == 4) //if state of connection is 4, indicates call is complete with all data recieved
				{
					//alert("on completion" + req.readyState);
					if (req.status == 200) //call success/failure: 200 indicates successful call
					{
						try
						{
							//alert(req.responseText);
							//alert(req.responseXML);

							//get result by tag name in returned message
							var recovered = req.responseXML.documentElement.getElementsByTagName("recoveredPassword");
							
							//get value as string from result object
							var recoveredPassword;
							recoveredPassword = recovered[0].innerHTML;

							//update GUI with recovered password
							document.getElementById("txt_RecoveredPassword").value = recoveredPassword;
						}
						catch (e)
						{
							alert("Failed to get recoveredpassword from XML response. Error is: \n" + e.message)
						}
					}
					else if (req.status == 400)
					{
						alert("Asynchronous call failed Request can not be understood by server: Malformed syntax")
					}
					else//status of call was not successful, indicate error and print error message
					{
						alert("Asynchronous call failed. ResponseText was: \n" + req.responseText);//get raw message text from responseText
					}
					req = null; //free up the connection by setting connection object to null
				}
			}

			//function called by recover button click
			function FindPassword()
			{
				try
				{
					FindPasswordAsync(FindPasswordRPC_OnCompoletion); //call async method with callback function 
				}
				catch (e)
				{
					alert("Failed to async call. Error is: \n" + e.message)
				}
			}
		</script>
	</head>

	<body>
		<form id="form1" runat="server">

			<asp:Label ID="lbl_PasswordLength" runat="server" style="top: 30px; left: 201px; position: absolute; height: 19px; width: 124px" Text="Password Length:"></asp:Label>
			<input id="txtBox_PasswordLength" runat="server" type="text" />
			<input id="btn_PasswordLengthUpdate" type="button" value="Update" runat="server" onserverclick="btn_PasswordLengthUpdate_ServerClick" />
	
			<input id="btn_NextPasswordToRecover" type="button" value="Next Password To Recover" runat="server" onserverclick="btn_NextPasswordToRecover_ServerClick" />
			<input id="txt_NextPasswordToRecover" type="text" runat="server"/>
			<asp:Label ID="lbl_NextPasswordEncrypted" runat="server" style="top: 116px; left: 153px; position: absolute; height: 19px; width: 168px" Text="Next Password Encrypted:"></asp:Label>
	
			<asp:Label ID="lbl_RunRecovery" runat="server" style="top: 183px; left: 280px; position: absolute; height: 19px; width: 86px" Text="Run Recovery"></asp:Label>
			<input id="btn_Recover" type="button" value="Recover" onclick="FindPassword()"/>

			<asp:Label ID="lbl_RecoveredPassword" runat="server" style="top: 232px; left: 211px; position: absolute; height: 19px; width: 147px" Text="Recovered Password"></asp:Label>
			<input id="txt_RecoveredPassword" runat="server" type="text" readonly/>

		</form>
	</body>
</html>
