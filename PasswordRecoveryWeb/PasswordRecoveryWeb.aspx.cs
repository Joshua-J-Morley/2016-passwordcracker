﻿using PasswordRecoveryBizDLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

///*****************************************************************************///
/// <summary> asp .NET web site. used by GUI to contact web service </summary>  ///
///*****************************************************************************///
public partial class _Default : Page
{
	///************************************************///
	///************************************************///
	///	      <summary> Member Fields </summary>       ///
	///************************************************///
	///************************************************///
	///web service member field, used to interact with manager
	private IPRWebService webService;


	///*******************************************************///
	///*******************************************************///
	///	      <summary> standard class methods </summary>     ///
	///*******************************************************///
	///*******************************************************///

	///*********************************************************************************///
	/// <summary> default constructor, initialises web service member field </summary>  ///
	///*********************************************************************************///
    protected void Page_Load(object sender, EventArgs e)
    {
		webService = new PRWebServiceImpl();
    }

	///****************************************************************************///
	///	  <summary> listener for update password length button on GUI </summary>   ///
	///****************************************************************************///
	protected void btn_PasswordLengthUpdate_ServerClick(object sender, EventArgs e)
	{
		int passwordLength;
		try
		{
			passwordLength = Int32.Parse(txtBox_PasswordLength.Value);

			///validate password length
			if (passwordLength < 1)
			{
				///indicate to user to enter valid number
				System.Windows.Forms.MessageBox.Show("Please Enter a number > 0");
			}
			else
			{
				//try set the password length
				try
				{
					webService.setPasswordSize(passwordLength);
				}
				//catch fault exception from server if methods fail
				catch (FaultException setLengthError)
				{
					Console.WriteLine("\n\n set length fault:\n" + setLengthError);
				}
				///Issue Communicating with manager
				catch (CommunicationObjectFaultedException setPasswordSize)
				{
					Console.WriteLine("\n\n  set password communication fault:\n" + setPasswordSize);
				}
			}
		}
		catch(FormatException formatException)
		{
			System.Windows.Forms.MessageBox.Show("Please Enter a number");
		}
		catch(ArgumentNullException nullException)
		{
			System.Windows.Forms.MessageBox.Show("Please Enter a number");
		}

		
	}

	///******************************************************************************///
	///	  <summary> listener for next password to recover button on GUI </summary>   ///
	///******************************************************************************///
	protected void btn_NextPasswordToRecover_ServerClick(object sender, EventArgs e)
	{
		string encryptedPassword;

		//try set the password length
		try
		{
			///get next password from web service and update txtbox on GUI
			encryptedPassword = webService.GenerateNextPasswordToRecover();
			txt_NextPasswordToRecover.Value = encryptedPassword;
		}
		//catch fault exception from server if methods fail
		catch (FaultException nextPasswordToRecover)
		{
			Console.WriteLine("\n\n next password to recover fault:\n" + nextPasswordToRecover);
		}
		///Issue Communicating with manager
		catch (CommunicationObjectFaultedException nextPasswordToRecover)
		{
			Console.WriteLine("\n\n  next password communication fault:\n" + nextPasswordToRecover);
		}
	}
}