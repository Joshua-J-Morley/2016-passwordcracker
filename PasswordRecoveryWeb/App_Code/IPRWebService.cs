﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

///******************************************************************************************///
///	<summary>  Interface for web service specifies service and operation contracts</summary> ///
///******************************************************************************************///
[ServiceContract]
public interface IPRWebService
{
	///****************************************************************************///
	///	     <summary> used to set password size in password manager </summary>    ///
	/// <param name="passwordSize"> validated number of chars in password </param> ///
	///****************************************************************************///
	[OperationContract]
	void setPasswordSize(int passwordSize);

	///****************************************************************************///
	///	  <summary> used to get next password to recover from manager </summary>   ///
	///         <returns> the next envrypted password to recover</returns>         ///
	///****************************************************************************///
	[OperationContract]
	string GenerateNextPasswordToRecover();

	///****************************************************************************///
	///	  <summary> used to call begin password recovery in manager </summary>     ///
	///   <param name="recoveredPassword"> plaintext password recovered</param>    ///
	///****************************************************************************///
	[OperationContract]
	void RunRecovery(out string recoveredPassword);
}
