﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using PasswordRecoveryBizDLL;

///**************************************************************************///
/// <summary> Web service class to connect Web browser and password manager
/// used by ASP .NET web site <see cref="PasswordRecoveryWeb.aspx.cs"/>
/// inherits web service interface and GUI callback interface </summary>
///**************************************************************************///

public class PRWebServiceImpl : IPRWebService, IManagerGuiCallback
{
	/// <summary> interface used to connect a GUI program to the Manager </summary>
	private IManagerGui mPassManager;


	///******************************************************************///
	///	     <summary> Default constructor for web service </summary>    ///
	///******************************************************************///
	public PRWebServiceImpl()
	{
		//try call method to set up connection to manager
		try
		{
			//connection code at bottom of file for better readability of other methods
			InitialiseManagerConnection();
		}
		//catch argument null exception
		catch (ArgumentNullException addressIsNull)
		{
			Console.WriteLine("\n\n address for channel factory is null:\n" + addressIsNull);
			
			//throw fault to asp site
			throw new FaultException("Address for channel factory is null");
		}
		//catch argument null exception
		catch (Exception otherExceptions)
		{
			Console.WriteLine("\n\n channel factory returns null:\n" + otherExceptions.Message);

			//throw fault to asp site
			throw new FaultException("Channel factory returned null");
		}
	}

	///****************************************************************************///
	///	     <summary> used to set password size in password manager </summary>    ///
	/// <param name="passwordSize"> validated number of chars in password </param> ///
	///****************************************************************************///
	public void setPasswordSize(int passwordSize)
	{
		int passwordLength;
		passwordLength = passwordSize;

		///attempt to set password in manager
		try
		{
			mPassManager.SetPasswordSize(passwordLength);
		}
		///FaultException thrown by manager
		catch (FaultException setPasswordSize)
		{
			Console.WriteLine("\n\n set password fault:\n" + setPasswordSize);

			///throw exception to asp web site 
			throw new FaultException("SetPasswordFault");
		}
		///Issue Communicating with manager
		catch (CommunicationObjectFaultedException setPasswordSize)
		{
			Console.WriteLine("\n\n  set password communication fault:\n" + setPasswordSize);

			///throw exception to asp web site 
			throw new CommunicationObjectFaultedException("SetPasswordFault");
		}
	}


	///****************************************************************************///
	///	  <summary> used to get next password to recover from manager </summary>   ///
	///         <returns> the next envrypted password to recover</returns>         ///
	///****************************************************************************///
	public string GenerateNextPasswordToRecover()
	{
		string encryptedPasswordToRecover = null;

		///attempt to get next password to recover
		try
		{
			encryptedPasswordToRecover = mPassManager.NextEncryptedPasswordToRecover();
		}
		///FaultException thrown by manager
		catch (FaultException nextPass)
		{
			Console.WriteLine("\n\n next encrypted password to recover fault:\n" + nextPass);

			///throw exception to asp web site 
			throw new FaultException("next password to recover fault");
		}
		///Issue Communicating with manager
		catch (CommunicationObjectFaultedException nextPass)
		{
			Console.WriteLine("\n\n   next encrypted password communication fault:\n" + nextPass);

			///throw exception to asp web site 
			throw new CommunicationObjectFaultedException(" next password Communication fault");
		}
		finally
		{
			//if exception occured and no value was returned
			if(encryptedPasswordToRecover == null)
			{
				encryptedPasswordToRecover = "Error";
			}
		}

		//return recieved next password to recover
		return encryptedPasswordToRecover;
	}


	///****************************************************************************///
	///	  <summary> used to call begin password recovery in manager </summary>     ///
	///   <param name="recoveredPassword"> plaintext password recovered</param>    ///
	///****************************************************************************///
	public void RunRecovery(out string recoveredPassword)
	{
		//default return string to Error, if successful will be updated to plaintext password
		recoveredPassword = null;

		///attempt to begin recovery
		try
		{
			//System.Windows.Forms.MessageBox.Show("calling begin recovery");
			recoveredPassword = mPassManager.BeginRecovery();
		}
		///FaultException thrown by manager
		catch (FaultException nextPassFault)
		{
			Console.WriteLine("recovery fault:\n" + nextPassFault);
		}
		///Issue Communicating with manager
		catch (CommunicationObjectFaultedException nextPassFault)
		{
			Console.WriteLine("recovery communication fault:\n" + nextPassFault);
		}
		finally
		{
			//if exception occured and no value was returned
			if (recoveredPassword == null)
			{
				recoveredPassword = "Error";
			}
		}
	}


	///**************************************************************************************///
	///<summary> initialise connection to manager via duplex channel/ nettcpbinding</summary>///
	///**************************************************************************************///
	private void InitialiseManagerConnection()
	{
		DuplexChannelFactory<IManagerGui> managerFactory;
		NetTcpBinding tcpBinding = new NetTcpBinding();
		IManagerGuiCallback cbHandler;

		//create reference to an instance of this class to be used as callback
		cbHandler = this;

		//increase max size of messages and number of elements in array
		tcpBinding.MaxReceivedMessageSize = System.Int32.MaxValue;
		tcpBinding.ReaderQuotas.MaxArrayLength = System.Int32.MaxValue; 

		string sURL = "net.tcp://localhost:5002/RecoveryManager1";

		//try create controller factory
		try
		{
			//duplex channel factory passing instance of this class as callback handler
			managerFactory = new DuplexChannelFactory<IManagerGui>(cbHandler, tcpBinding, sURL);

			//ensure passControllerFactory sucessfully initialized
			if (managerFactory != null)
			{
				//create channel to initialize passControllerFactory
				mPassManager = managerFactory.CreateChannel();

				//change timeout time to account for larger password recoveries
				IClientChannel contextChannel = mPassManager as IClientChannel;
				contextChannel.OperationTimeout = TimeSpan.FromDays(10);
			}
		}
		//catch argument null exception
		catch (ArgumentNullException addressIsNull)
		{
			Console.WriteLine("\n\n address for channel factory is null:\n" + addressIsNull);

			///throw exception to constructor
			throw new ArgumentNullException("address for channel factory is null");
		}
		//catch argument null exception
		catch (Exception otherExceptions)
		{
			Console.WriteLine("\n\n channel factory returns null:\n" + otherExceptions.Message);

			///throw exception to constructor
			throw new Exception("Manager communication connection failed");
		}
	}


	///**********************************************************************************///
	///     <summary>dummy method required to inherit IManagerGuiCallback </summary>     ///
	///**********************************************************************************///
	public void NotifyGui(double inProcessed, double inTotal) { }
}
