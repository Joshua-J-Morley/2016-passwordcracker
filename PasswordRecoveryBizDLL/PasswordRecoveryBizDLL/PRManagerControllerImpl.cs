﻿using PwdRecover;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Runtime.CompilerServices;
using System.Runtime.Remoting.Messaging;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PasswordRecoveryBizDLL
{
	///delegate used to recover password asynchronously
	public delegate string RecoverPasswordDelegate();
	///delegate used to generate permutations of letters asynchronously
	public delegate void GenerateCombinationsDelegate();

	///*****************************************************************************************///
	/// <summary> password recovery manager. This manager recieves requests from
	/// presentation tier systems (Windows GUI and ASP .NET Ajax web site and delegates
	/// work to worker services who register themselves in this manager </summary>
	/// <remarks> manager is singleton and allows multiple threads running concurrently</remarks>
	///*****************************************************************************************///
	[ServiceBehavior(InstanceContextMode = InstanceContextMode.Single,
					ConcurrencyMode = ConcurrencyMode.Multiple,
					UseSynchronizationContext = false)]
	public class PRManagerControllerImpl : IPRManagerController
	{
		///*****************************************************///
		///*****************************************************///
		///	<summary> Password finding member fields </summary> ///
		///*****************************************************///
		///*****************************************************///
		///member field for password services DLL
		private PasswordServices mPasswordServices;
		///length in characters of password to be cracked
		private int mPasswordSize;
		///string storing encrypted password to crack
		private string mEncryptedPassword;
		///string storing unencrypted password when found
		private string mUnecncryptedPassword;
		///constant string storing possible characters contained in password
		private const string ALPHANUM = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";


		///*************************************************************///
		///*************************************************************///
		///	<summary> Synchronization booleans Member fields </summary> ///
		///*************************************************************///
		///*************************************************************///
		///boolean indicating if password has been found
		private bool mPasswordFound;
		///poisen pill to stop generating passwords if password has been found
		private bool mStopGenerating;
		///control flag to handle generation complete/password found race condition
		private bool mGeneratingFinished;


		///**********************************************************///
		///**********************************************************///
		///	<summary> Possible combinations member fields </summary> ///
		///**********************************************************///
		///**********************************************************///
		///queue of all possible combinations 
		private Queue<List<string>> mCombinationQueue;
		///mSegment size is the size of batches possible passwords will be split into
		private int mSegmentSize;
		///total number of segements
		private long mSegmentsTotal;
		///number of segments processed
		private long mSegmentsProcessed;
		///number of segments currently held in memory
		private int mSegmentsInMemory;
		///used as running total when dividing possible passwords into batches
		private int mCurrentChunkSize;
		///percentage of segments/combinations that have been checked
		private int mPercentComplete;
		

		///***************************************************************///
		///***************************************************************///
		///	      <summary> Worker/GUI server member fields </summary>     ///
		///	<remarks> Multiple gui callbacks is future feature</remarks>  ///
		///***************************************************************///
		///***************************************************************///
		//list of server worker callbacks used to communicate with workers
		private List<IWorkerCallback> mWorkerList;


		///*******************************************************///
		///*******************************************************///
		///	      <summary> standard class methods </summary>     ///
		///*******************************************************///
		///*******************************************************///

		///********************************************************************///
		///   <summary> Default constructor for password manager </summary>    ///
		///********************************************************************///
		public PRManagerControllerImpl()
		{
			///initialise Password finding member fields
			mPasswordServices = new PasswordServices();
			mPasswordSize = 3;
			mEncryptedPassword = "";
			mUnecncryptedPassword = "";

			///initialise synchronization booleans 
			mPasswordFound = false;
			mStopGenerating = false;
			mGeneratingFinished = false;

			///initialise possible combinations member fields
			mCombinationQueue = new Queue<List<string>>();
			mSegmentSize = 100;
			mSegmentsTotal = 0;
			mSegmentsProcessed = 0;
			mSegmentsInMemory = 0;
			mCurrentChunkSize = 0;
			mPercentComplete = 0;

			///initialise Worker/GUI server member fields
			mWorkerList = new List<IWorkerCallback>();
		}

		///***********************************************************///
		///   <summary> destructor for password manager </summary>    ///
		///***********************************************************///
		~PRManagerControllerImpl()
		{

		}

		
		///************************************************///
		///************************************************///
		///	<summary> Password Recovery Methods </summary> ///
		///************************************************///
		///************************************************///

		///*******************************************************************************///
		/// <summary> used to get an ecrpyted password from the dll to recover </summary> ///
		///         <returns> the next envrypted password to recover   </returns>         ///
		///*******************************************************************************///
		public string NextEncryptedPasswordToRecover()
		{
			string retString;

			retString = mPasswordServices.NextEncryptedPasswordToRecover(mPasswordSize);

			//Console.WriteLine("PAssword: " + retString);

			///update member fields
			SetEncryptedPassword(retString);
			SetPasswordFoundFalse();

			return retString;
		}

		///**********************************************************************************///
		///<summary> begin recovering password. Async calls generate combinations </summary> ///
		///            <returns> the unencryped password when recovered </returns>           ///
		///**********************************************************************************///
		public string BeginRecovery()
		{
			///initialise / reset variables for recovery commencement
			bool combinationsFinished = false;

			GenerateCombinationsDelegate generateDel;
			AsyncCallback asyncCallback;

			mSegmentsProcessed = 0;
			mSegmentsTotal = 0;
			mPasswordFound = false;
			mPercentComplete = 0;
			mSegmentsInMemory = 0;
			mCombinationQueue.Clear();

			///begin asyncronously generating possible combinations of passwords
			generateDel = this.GenerateCombinations;
			asyncCallback = this.CombinationsGeneratedComplete;
			generateDel.BeginInvoke(asyncCallback, null);

			///try calcualte the number of segments
			try
			{
				///segment size is defaulted to 10 000, this may be optimized in future
				mSegmentsTotal = (long)(BigInteger.Pow(62, mPasswordSize)) / mSegmentSize;
				
				///if there are less than 10 000 possible combinations
				if (mSegmentsTotal < 1)
				{
					///set segments total value to the number of combinations
					mSegmentsTotal = (long)(BigInteger.Pow(62, mPasswordSize));
					mSegmentSize = (int)mSegmentsTotal;
				}
			}
			///catch an overflow exception
			///this should not happen as programs limit is size 10 passwords
			catch (OverflowException SegmentsTotaloverflow)
			{
				Console.WriteLine(SegmentsTotaloverflow.Message);
			}

			///loop until all combinations have been checked or password has been found
			while (combinationsFinished == false && mPasswordFound == false)
			{
				///while there are still workers to perform request and password has not been found
				while (mWorkerList.Count > 0 && mPasswordFound == false)
				{
					///each iteration set callback to null to prevent dangling references
					IWorkerCallback callback = null;

					///if list of workers is not empty and there are combinations to check
					if (mWorkerList.Count > 0 && mCombinationQueue.Count > 0)
					{
						///take out the first worker from the list
						callback = mWorkerList.First();
						
					}

					///recheck there are still combinations and check worker callback recieved
					if (mCombinationQueue.Count > 0 && callback != null)
					{
						///take out batch of passwords and pass to worker
						List<string> passwords = mCombinationQueue.Dequeue();
						try
						{
							callback.FindPasswordAsync(passwords, mEncryptedPassword);

							///this worker has its allocation, 
							///so remove it from the list  of available workers
							mWorkerList.RemoveAt(0);
							mSegmentsInMemory--;

							///error handling, if password not found but all passwords checked
							if (mSegmentsProcessed >= mSegmentsTotal)
							{
								///used to exit loop
								combinationsFinished = true;
							}
						}
						///catch communication failed exception
						catch (ObjectDisposedException objectDisposedException)
						{
							Console.Write("worker communicaiton disposed");
							Console.WriteLine(objectDisposedException.Message);
						}
						///catch if worker is in faulted state
						catch (CommunicationObjectFaultedException communicationObjectException)
						{
							Console.Write("worker communicaiton faulted");
							Console.WriteLine(communicationObjectException.Message);
						}

					}

					///update percent completed
					double pCompl = (((double)mSegmentsProcessed / (double)mSegmentsTotal) * 100);
					mPercentComplete = (int)pCompl;
					
					///if percent complete as integer is 0 (common larger passwords)
					if (mPercentComplete == 0)
					{
						///set to 1 to indicate on GUI that processing is occuring
						mPercentComplete = 1;
					}
				}
			}
			///password was not found successfully
			if (mPasswordFound == false)
			{
				//update values for next recovery and user feedback
				mSegmentsProcessed = 0;
				mPercentComplete = 100;
				Console.WriteLine("\n\nfinished searching Password has not been found\n\n");
			}
			///password was found successfully
			else
			{
				//update values for next recovery and user feedback
				mSegmentsProcessed = 0;
				mPercentComplete = 100;
				///Console.WriteLine("\n\nfinished searching Password found successfully\n\n");
			}

			return mUnecncryptedPassword;
		}


		///****************************************************************************///
		///	     <summary> called by worker when a segment has completed </summary>    ///
		///      <param name="result"> either recovered password or null </param>      ///
		///****************************************************************************///
		public void OnRecoveryComplete(string result)
		{
			///if result value is not null, password has been recovered
			if (result != null)
			{
				///indicate password has been found and update member field
				mPasswordFound = true;
				mUnecncryptedPassword = result;
				mPercentComplete = 100;
			}

			///indicate another segment has been processed and readd worker to list
			mSegmentsProcessed++;
			mWorkerList.Add(OperationContext.Current.GetCallbackChannel<IWorkerCallback>());

			//Console.WriteLine("worker: " + mWorkerList.Count + " is no longer busy");
		}

		///*********************************************************************************///
		///  <summary> used to set passwordfound member to true to stop recovery </summary> ///
		///*********************************************************************************///
		public void CancelOperation()
		{
			mPasswordFound = true;
		}


		///****************************************************///
		///****************************************************///
		///	<summary> Generate combinations methods </summary> ///
		///****************************************************///
		///****************************************************///

		///*********************************************************************///
		///	     <summary> used to generate password combinations </summary>    ///
		///*********************************************************************///
		[MethodImpl(MethodImplOptions.Synchronized)]
		public void GenerateCombinations()
		{
			///<summary> This loop is a spinlock to ensure a racecondition is handled </summary>
			///<remarks> Due to generation and recovery occuring asynchronously, it is 
			///possible for a passwod to be found and the user to begin another recovery
			///before the previous recovery generation has finished. this spinlock ensures
			///that this race condition does not impact on the program </remarks>
			while (mStopGenerating == true)
			{
				///could have had empty loop, but sleeping the thread for a small amount
				///of time is better on resource consumption
				Thread.Sleep(250);
			}
			///ensure combination queue is empty before generation
			mCombinationQueue.Clear();
			mGeneratingFinished = false;

			///create new empty chunk and call recursive method
			List<string> chunkOne = new List<string>();
			PermutationRecurse("", mPasswordSize, chunkOne);

			///indicate to console generation has been complete
			Console.WriteLine("generation complete");
			
			///reset chunksize, revert stopGenerating flag to default
			///and indicate generation has finished
			mCurrentChunkSize = 0;
			mStopGenerating = false;
			mGeneratingFinished = true;
		}

		
		///*************************************************************************************///
		///   <summary> will recursively build words of each combination of letter </summary>   ///
		/// <param name="inWord"> string that will cumulatively build to a combination </param> ///
		/// <param name="inPosition"> current position of character to be permuted </param>     ///
		/// <param name="inChunk"> cumulatively used to build chunk into segment size </param>  ///
		///*************************************************************************************///
		[MethodImpl(MethodImplOptions.Synchronized)]
		private void PermutationRecurse(string inWord, int inPosition, List<string> inChunk)
		{
			///poisen pill used when password has been recovered before generation has completed
			if (mStopGenerating == false)
			{
				/// if position is 0 combination is complete
				if (inPosition == 0)
				{
					///add this word to chunk and update chunk size
					inChunk.Add(inWord);
					mCurrentChunkSize++;
					//Console.WriteLine("chunk count" + chunkSize);

					///if chunksize is at semgent size
					if (mCurrentChunkSize == mSegmentSize)
					{
						///add copy of chunk as segment to queue of all combinations
						mCombinationQueue.Enqueue(new List<string>(inChunk));
						mSegmentsInMemory++;

						///reset chunk to empty
						inChunk.Clear();
						mCurrentChunkSize = 0;

						///used to control amount of data in memory
						if (mSegmentsInMemory >= 100)
						{
							///freeze generation of segments until there is space in memory
							while (mSegmentsInMemory > 100 && mStopGenerating == false)
							{
								Thread.Sleep(800);//sleep while there are max segments in memory
							}
						}
					}
				}
				///if position is not at 0, keep building combination
				else
				{
					///for each alphanumerical letter
					for (int ii = 0; ii < ALPHANUM.Length && mStopGenerating == false; ++ii)
					{
						///get new letter to add to end of combination
						char letter = ALPHANUM[ii];

						/// add new letter to current combination
						string newPrefix = inWord + letter;

						/// recurse in with next position in combination
						PermutationRecurse(newPrefix, inPosition - 1, inChunk);
					}
				}
			}
			///generation has been told to stop, reset current chunk size
			else
			{
				///before all combinations created, password was found
				mCurrentChunkSize = 0;
			}

		}


		///************************************************************************************///
		/// <summary> callback method for when combination generation has completed </summary> ///
		/// <param name="iResult"> object that stores AsyncResult object and delegate</param>  ///
		///************************************************************************************///
		public void CombinationsGeneratedComplete(IAsyncResult iResult)
		{
			///get async result object from imported interface
			GenerateCombinationsDelegate genDel;
			AsyncResult asyncRes = (AsyncResult)(iResult);

			///ensure endinvoke has not been already called
			if (asyncRes.EndInvokeCalled == false)
			{
				///get delegate out of asyncResult object and end the invokation
				genDel = (GenerateCombinationsDelegate)asyncRes.AsyncDelegate;
				genDel.EndInvoke(asyncRes);//outof memoryexception
			}
			///close async wait handle
			asyncRes.AsyncWaitHandle.Close(); 
		}


		///**********************************************************************///
		///**********************************************************************///
		///	<summary> Register Workers and GUI and give notifications </summary> ///
		///**********************************************************************///
		///**********************************************************************///

		///*****************************************************************************///
		/// <summary> adds a worker to list of workers from callback channel </summary> ///
		///*****************************************************************************///
		public void AddWorker()
		{
			///get worker object from channel that called this method
			IWorkerCallback newServicer;
			newServicer = OperationContext.Current.GetCallbackChannel<IWorkerCallback>();

			///ensure no duplicates will be in list
			if (!mWorkerList.Contains(newServicer))
			{
				///add worker to list and indicate to user
				mWorkerList.Add(newServicer);
				Console.WriteLine("Added correctly, number: " + mWorkerList.Count);
			}
		}

		///**********************************************************************///
		///       <summary> removes a worker from list of workers </summary>     ///
		///**********************************************************************///
		public void RemoveWorker()
		{
			///get worker object from channel that called this method
			IWorkerCallback newServicer;
			newServicer = OperationContext.Current.GetCallbackChannel<IWorkerCallback>();

			///ensure this worker is in the list of workers
			if (mWorkerList.Contains(newServicer))
			{
				///remove worker from list of workers and indicate to user
				mWorkerList.Remove(newServicer);
				Console.WriteLine("removed correctly");
			}
		}

		///*******************************************************************************///
		///<summary> will periodically update calling GUI of recovery progress </summary> ///
		///*******************************************************************************///
		public void SubscribeNotifications()
		{
			///get GUI object that called this method
			IManagerGuiCallback guiCallback;
			guiCallback = OperationContext.Current.GetCallbackChannel<IManagerGuiCallback>();

			///until passowrd has been recovered
			while (mPercentComplete < 100)
			{
				///notify the GUI of the progress via callback
				guiCallback.NotifyGui((double)mSegmentsProcessed, (double)mSegmentsTotal);

				///to prevent too much resource consumption, sleep thread
				Thread.Sleep(800);
			}

			///update percent complete
			mPercentComplete = 0;

			///if combination generation has not finished
			if (mGeneratingFinished == false)
			{
				///indicate to stop generating
				mStopGenerating = true;
			}
			
			///update GUI with 100% completion
			guiCallback.NotifyGui((double)mSegmentsTotal, (double)mSegmentsTotal);
		}


		///*********************************************///
		///*********************************************///
		///	<summary> Accessors and Mutators </summary> ///
		///*********************************************///
		///*********************************************///

		///****************************************************************************///
		///	            <summary> mutator for mEncryptedPassword </summary>            ///
		///  <param name="inEncryptedPassword"> encryptedPassword to recover </param>  ///
		///****************************************************************************///
		public void SetEncryptedPassword(string inEncryptedPassword)
		{
			mEncryptedPassword = inEncryptedPassword;
		}

		///******************************************************************************///
		///	              <summary> mutator for mPasswordSize </summary>                 ///
		/// <param name="inPasswordSize"> validated number of chars in password </param> ///
		///******************************************************************************///
		public void SetPasswordSize(int inPasswordSize)
		{
			mPasswordSize = inPasswordSize;

			///default segment size 10000
			mSegmentSize = 10000;
		}

		///******************************************************************///
		///	     <summary> used to set mPasswordFound to false </summary>    ///
		///	     <remarks> This is used to cancel a recovery job </remarks>  ///
		///******************************************************************///
		public void SetPasswordFoundFalse()
		{
			mPasswordFound = false;
		}

		///***********************************************************************///
		///        <summary> used to add a gui as member field </summary>         ///
		/// <remarks> This is future feature to support multiple Guis </remarks>  ///
		///************************************************************************///
		//public void AddGui()
		//{
		//	IManagerGuiCallback guiCallback;
		//	guiCallback = OperationContext.Current.GetCallbackChannel<IManagerGuiCallback>();

		//	mGui = guiCallback;
		//}

		///**************************************************************************///
		///        <summary> used to remove a gui as member field </summary>         ///
		///   <remarks> This is future feature to support multiple Guis </remarks>   ///
		///**************************************************************************///
		//public void RemoveGui()
		//{
		//	mGui = null;
		//}
	}

}
