﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace PasswordRecoveryBizDLL
{
	//////***********************************************************************************///
	///**************************************************************************************///
	///       <summary>  Interfaces for communication between Manager and GUI     </summary> ///
	///**************************************************************************************///
	//////***********************************************************************************///

	///***********************************************************************************///
	///	<summary>  Interface for guis to implement so manager can contact them </summary> ///
	///***********************************************************************************///
	[ServiceContract]
	public interface IManagerGuiCallback
	{
		///*****************************************************************************///
		///	       <summary> used to notify gui of recovery progress </summary>         ///
		/// <param name="inProcessed"> number of segments/combinations checked </param> ///
		///   <param name="inTotal"> total number of segments or combinations </param>  ///
		///   <remarks> Segments is used as unit of measurement unless password length
		///   is 1 or 2 characters for segment size of 10 000 </remarks>                ///
		///*****************************************************************************///
		[OperationContract]
		void NotifyGui(double inProcessed, double inTotal);
	}

	///*********************************************************************************///
	///	<summary>  Interface for manager to implement so guis can contact it </summary> ///
	///*********************************************************************************///
	[ServiceContract(CallbackContract = typeof(IManagerGuiCallback))]
	public interface IManagerGui
	{
		///*******************************************************************************///
		/// <summary> used to get an ecrpyted password from the dll to recover </summary> ///
		///         <returns> the next envrypted password to recover   </returns>         ///
		///*******************************************************************************///
		[OperationContract]
		string NextEncryptedPasswordToRecover();

		///**********************************************************************************///
		///<summary> begin recovering password. Async calls generate combinations </summary> ///
		///            <returns> the unencryped password when recovered </returns>           ///
		///**********************************************************************************///
		[OperationContract]
		string BeginRecovery();

		///*********************************************************************************///
		///  <summary> used to set passwordfound member to true to stop recovery </summary> ///
		///*********************************************************************************///
		[OperationContract]
		void CancelOperation();

		///*******************************************************************************///
		///<summary> will periodically update calling GUI of recovery progress </summary> ///
		///*******************************************************************************///
		[OperationContract]
		void SubscribeNotifications();

		///****************************************************************************///
		///	            <summary> mutator for mEncryptedPassword </summary>            ///
		///  <param name="inEncryptedPassword"> encryptedPassword to recover </param>  ///
		///****************************************************************************///
		[OperationContract]
		void SetEncryptedPassword(string inEncryptedPassword);

		///******************************************************************************///
		///	              <summary> mutator for mPasswordSize </summary>                 ///
		/// <param name="inPasswordSize"> validated number of chars in password </param> ///
		///******************************************************************************///
		[OperationContract]
		void SetPasswordSize(int inPasswordSize);

		///******************************************************************///
		///	     <summary> used to set mPasswordFound to false </summary>    ///
		///	     <remarks> This is used to cancel a recovery job </remarks>  ///
		///******************************************************************///
		[OperationContract]
		void SetPasswordFoundFalse();
	}


	///***********************************************************************************///
	///***********************************************************************************///
	///    <summary>  Interfaces for communication between Manager and Workers </summary> ///
	///***********************************************************************************///
	///***********************************************************************************///

	///**************************************************************************************///
	///	<summary>  Interface for workers to implement so manager can contact them </summary> ///
	///**************************************************************************************///
	[ServiceContract]
	public interface IWorkerCallback
	{
		///******************************************************************************///
		/// <summary> called by manager used to begin recovery asynchronously </summary> ///
		/// <param name="passwords">list of permutations of possible passwords </param>  ///
		///  <param name="encrypted">encrypted password to compare and recover</param>   ///
		///  <remarks> Method is called by manager and will asynchronously locally call 
		///  FindPassword so that managers network call can return and manager
		///  can continue execution without any asynchronous calls over network</remarks>///
		///******************************************************************************///
		[OperationContract]
		void FindPasswordAsync(List<string> passwords, string encrypted);
	}

	///************************************************************************************///
	///	<summary>  Interface for manager to implement so workers can contact it </summary> ///
	///************************************************************************************///
	[ServiceContract(CallbackContract = typeof(IWorkerCallback))]
	public interface IWorker
	{
		///*****************************************************************************///
		/// <summary> adds a worker to list of workers from callback channel </summary> ///
		///*****************************************************************************///
		[OperationContract]
		void AddWorker();

		///**********************************************************************///
		///       <summary> removes a worker from list of workers </summary>     ///
		///**********************************************************************///
		[OperationContract]
		void RemoveWorker();

		///****************************************************************************///
		///	     <summary> called by worker when a segment has completed </summary>    ///
		///      <param name="result"> either recovered password or null </param>      ///
		///****************************************************************************///
		[OperationContract]
		void OnRecoveryComplete(string result);
	}


	///***********************************************************************************///
	///***********************************************************************************///
	///                     <summary>  Interfaces for manager </summary>                  ///
	/// <remarks> In order to increase encapsulation and enfore separation of concerns 
	/// the interface classes for GUI-Manager comm and Worker-Manager comm are separated
	/// however .NET enforces serviceContract callback rules that mean the managers
	/// interface and the managers interface callback must inherit from the other
	/// interfaces and their callbacks respecively. More details in report </remarks>     ///
	///***********************************************************************************///
	///***********************************************************************************///
	
	///*********************************************************************************///
	///	<summary>  Callback Interface for manager, implements other callbacks</summary> ///
	///*********************************************************************************///
	[ServiceContract]
	public interface IPRManagerControllerCallback : IWorkerCallback, IManagerGuiCallback
	{

	}

	///************************************************************************************///
	///	<summary>  Interface for manager to implement so workers can contact it </summary> ///
	///************************************************************************************///
	[ServiceContract(CallbackContract = typeof(IPRManagerControllerCallback))]
	public interface IPRManagerController : IWorker, IManagerGui
	{
		///*********************************************************************///
		///	     <summary> used to generate password combinations </summary>    ///
		///*********************************************************************///
		[OperationContract]
		void GenerateCombinations();
	}
}
