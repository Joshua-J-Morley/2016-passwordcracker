To Launch Windows GUI:

0: Navigate to 15490010_SoftwareComponents\PasswordRecoveryBiz\PasswordRecoveryBiz\bin\Debug
1: Execute: PasswordRecoveryBiz.exe
2: Execute at least 1: PasswordRecoveryBizService.exe
3: Execute: PasswordRecoveryGui.exe
4: On the menu window Enter a password Length and click "Update"
5: click "Next Password To Recover"
6: Click "Recover"
6A: Wait for password to be recovered
6B: Click Cancel

To Repeat, 
	Start from step 4 if you would like to change length
	Start from step 5 if you would not like to change length

To exit:
If Recovery is running, Click cancel
close all PasswordRecoveryBizService.exe
then close MainWindow, 
then close PasswordRecoveryBiz



To Launch Website:
0: Follow Steps 0-2 inclusive from above
1: Open Visual Studio
2: select File: Open, Website
3: Navigate to \15490010_SoftwareComponents\ and select the PassowrdRecoveryWeb Directory
4: click "Open"
5: on the side bar "solution Explorer", double click on "PasswordRecoveryWeb.aspx"
6: Click the "Run" button
7: follow steps 4-6a inclusive from above

To Exit: 
Close the window
press the red Stop button in visual studio




Markers Note:
For some reason workers randomly began dropping out (due to some obscure problem) the day before assignment was due,
I have spoken about this in the report
Note that the functionality of the program still works, when workers time out, you simply need to create new workers, so this bug does not impact the system working. 