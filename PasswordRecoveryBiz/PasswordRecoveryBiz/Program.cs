﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using PasswordRecoveryBizDLL;

namespace PasswordRecoveryBiz
{
	///**************************************************************///
	/// <summary> class containing main for the manager </summary>   ///
	/// <remarks> creates endpoints for GUI and workers to
	/// connect to then passes requests on to the DLL</remarks>      ///
	///**************************************************************///
	class Program
	{
		static void Main(string[] args)
		{
			ServiceHost host;
			NetTcpBinding tcpBinding = new NetTcpBinding();

			///.net has restrictions on max size of message and num elements in array
			///increase max size of messages
			tcpBinding.MaxReceivedMessageSize = System.Int32.MaxValue; 
			///increase max number of elements in array
			tcpBinding.ReaderQuotas.MaxArrayLength = System.Int32.MaxValue;

			//host implementation class
			host = new ServiceHost(typeof(PRManagerControllerImpl)); 
	
			///url for Guis to connect to
			string sURL1 = "net.tcp://localhost:5002/RecoveryManager1";
			///url for workers to connect to
			string sURL2 = "net.tcp://localhost:5002/RecoveryManager2";

			// server services for GUIs will be accessed via interface class
			host.AddServiceEndpoint(typeof(IManagerGui), tcpBinding, sURL1);
			// server services for workers will be accessed via interface class
			host.AddServiceEndpoint(typeof(IWorker), tcpBinding, sURL2); 

			host.Open(); //enter listening state ready for client to make requests
			Console.WriteLine("Press Enter to Exit"); //block main finishing until user enters
			Console.ReadLine();
			host.Close(); //close ServiceHost
		}
	}
}
